Name:     mdadm
Version:  4.2
Release:  18
Summary:  The software RAID arrays user manage tools
License:  GPLv2+
URL:      http://www.kernel.org/pub/linux/utils/raid/mdadm/

Source0:  http://www.kernel.org/pub/linux/utils/raid/mdadm/mdadm-%{version}.tar.xz
Source1:  mdcheck-cron
Source2:  mdmonitor.service
Source3:  mdadm.conf

Patch1:  0001-mdadm-remove-Werror-to-fix-Werror-address-of-packed-.patch
Patch2:  0002-mdadm-Fix-mdadm-r-remove-option-regresision.patch
Patch3:  0003-monitor-Avoid-segfault-when-calling-NULL-get_bad_blo.patch
Patch4:  0004-mdadm-mdcheck_start.service-mdcheck_continue.service.patch
Patch5:  0005-Fix-possible-NULL-ptr-dereferences-and-memory-leaks.patch
Patch6:  0006-mdadm-Don-t-open-md-device-for-CREATE-and-ASSEMBLE.patch
Patch7:  0007-DDF-Fix-NULL-pointer-dereference-in-validate_geometr.patch
Patch8:  0008-fix-NULL-dereference-in-super_by_fd.patch
Patch9:  0009-fix-mdmonitor-oneshot.service-start-error.patch
Patch10: 0010-Fix-null-pointer-for-incremental-in-mdadm.patch
Patch11: 0011-Manage-Block-unsafe-member-failing.patch
Patch12: 0012-Manage-do-not-check-array-state-when-drive-is-remove.patch
Patch13: 0013-incremental-manage-do-not-verify-if-remove-is-safe.patch
Patch14: 0014-Fix-race-of-mdadm-add-and-mdadm-incremental.patch
Patch15: 0015-mdadm-Fix-double-free.patch
Patch16: 0016-Mdmonitor-Fix-segfault.patch
Patch17: 0017-mdmon-fix-segfault.patch
Patch18: 0018-fix-memory-leak-in-file-mdadm.patch
Patch19: 0019-Fix-memory-leak-in-file-Manage.patch
Patch20: 0020-Manage-fix-check-after-dereference-issue.patch
Patch21: 0021-mdadm-Assemble.c-fix-coverity-issues.patch
Patch22: 0022-config.c-Fix-memory-leak-in-load_containers.patch
Patch23: 0023-mapfile.c-Fix-STRING_OVERFLOW-issue.patch
Patch24: 0024-mdadm-Monitor.c-fix-coverity-issues.patch
Patch25: 0025-mdadm-lib.c-fix-coverity-issues.patch
Patch26: 0026-mdadm-msg.c-fix-coverity-issues.patch
Patch27: 0027-mdadm-mdopen-fix-coverity-issue-CHECKED_RETURN.patch
Patch28: 0028-mdadm-super1-fix-coverity-issue-CHECKED_RETURN.patch
Patch39: 0029-mdadm-util.c-fix-coverity-issues.patch
Patch30: 0030-mdadm-sysfs.c-fix-coverity-issues.patch

BuildRequires:    systemd gcc binutils libudev-devel

Requires(post):   systemd coreutils
Requires(preun):  systemd
Requires(postun): systemd coreutils

%description
mdadm is a tool for managing Linux Software RAID arrays.
It can create, assemble, report on, and monitor arrays.
It can also move spares between raid arrays when needed.

%package        help
Summary:        Including man files for mdadm
Requires:       man
BuildArch:      noarch

%description    help
This contains man files for the using of mdadm.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%make_build CXFLAGS="$RPM_OPT_FLAGS" LDFLAGS="$RPM_LD_FLAGS" SYSCONFDIR="%{_sysconfdir}" mdadm mdmon

%check
make test

%install
make DESTDIR=%{buildroot} MANDIR=%{_mandir} BINDIR=%{_sbindir} SYSTEMD_DIR=%{_unitdir} install install-systemd
install -Dp -m 755 misc/mdcheck %{buildroot}%{_sbindir}/mdcheck
install -Dp -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/cron.d/mdcheck

#install mdmonitor.service from local file
install -D -m 644 %{SOURCE2} %{buildroot}%{_unitdir}

mkdir -p %{buildroot}%{_tmpfilesdir}
install -m 644  %{SOURCE3} %{buildroot}%{_tmpfilesdir}/mdadm.conf
install -d -m 710 %{buildroot}/var/run/mdadm/

%post
%systemd_post mdmonitor.service
/usr/bin/systemctl disable mdmonitor-takeover.service  >/dev/null 2>&1 || :

%preun
%systemd_preun mdmonitor.service

%postun
%systemd_postun_with_restart mdmonitor.service

%files
%doc ChangeLog  mdadm.conf-example
%license COPYING
%{_udevrulesdir}/*
%{_sbindir}/*
%{_unitdir}/*
/usr/lib/systemd/system-shutdown/mdadm.shutdown
%config(noreplace) %{_sysconfdir}/cron.d/mdcheck
%config(noreplace) %{_tmpfilesdir}/mdadm.conf 
%dir %{_localstatedir}/run/mdadm/

%files help
%{_mandir}/man*/*

%changelog
* Mon Aug 26 2024 wuguanghao <wuguanghao3@huawei.com> - 4.2-18
- backport bugfix patches from community

* Wed Jul 17 2024 wuguanghao <wuguanghao3@huawei.com> - 4.2-17
- mdmonitor.service: add conditional judgment to avoid service startup failed

* Mon Jun 24 2024 wuguanghao <wuguanghao3@huawei.com> - 4.2-16
- Manage: fix check after dereference issue

* Thu Jun 6 2024 zhangyaqi <zhangyaqi@kylinos.cn> - 4.2-15
- fix memory leak in file mdadm

* Wed May 15 2024 Deyuan Fan <fandeyuan@kylinos.cn> - 4.2-14
- mdmon: fix segfault

* Wed May 8 2024 Deyuan Fan <fandeyuan@kylinos.cn> - 4.2-13
- Mdmonitor: Fix segfault

* Thu Apr 18 2024 liuh <liuhuan01@kylinos.cm> - 4.2-12
- sync patch from community

* Tue Dec 19 2023 wuguanghao <wuguanghao3@huawei.com> - 4.2-11
- Fix race of "mdadm --add" and "mdadm --incremental"

* Tue Nov 7 2023 wuguanghao <wuguanghao3@huawei.com> - 4.2-10
* fix RAID0 becoming unusable after setting fault of member disks

* Wed Sep 13 2023 miaoguanqin <miaoguanqin@huawei.com> - 4.2-9
* Fix null pointer for incremental in mdadm

* Fri Jun 9 2023 miaoguanqin <miaoguanqin@huawei.com> - 4.2-8
* fix mdmonitor-oneshot.service

* Mon Apr 24 2023 tangyuchen <tangyuchen5@huawei.com> - 4.2-7
* fix crond cannot find command mdadm

* Fri Jan 6 2023 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 4.2-6
* fix NULL dereference in super_by_fd

* Fri Dec 16 2022 Lixiaokeng <lixiaokeng@huawei.com> - 4.2-5
* fix NULL dereference

* Thu Dec 8 2022 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 4.2-4
- backport two bugfix patches

* Fri Dec 2 2022 miaoguanqin <miaoguanqin@huawei.com> - 4.2-3
- mdadmcheck_start.service mdadm_continue.service error

* Mon Nov 14 2022 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 4.2-2
- backport upstream bugfix patch to fix segfault problem

* Mon Oct 17 2022 wuguanghao <wuguanghao3@huawei.com> - 4.2-1
- upgrade version to 4.2

* Sat Jun 25 2022 wuguanghao <wuguanghao3@huawei.com> - 4.1-6
- fix segfault of --monitor -r

* Wed Nov 4 2020 lixiaokeng<lixiaokeng@huawei.com> - 4.1-5
- add make test

* Thu Sep 24 2020 wuguanghao <wuguanghao3@huawei.com> - 4.1-4
- fix mdmonitor.service start failed 

* Mon Jul 13 2020 Zhiqiang Liu <lzhq28@mail.ustc.edu.cn> - 4.1-3
- backport upstream bugfix patches

* Sun Jul 5 2020 Zhiqiang Liu <lzhq28@mail.ustc.edu.cn> - 4.1-2
- remove useless readme files.

* Tue Jun 30 2020 wuguanghao<wuguanghao3@huawei.com> - 4.1-1
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC: update mdadm version to 4.1-1 

* Mon Jun 29 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 4.1-rc2.0.11
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC: renumber patches

* Mon Jun 29 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 4.1-rc2.0.10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: remove -Werror to fix [-Werror=address-of-packed-member] problem

* Mon Mar 9 2020 hy <eulerstoragemt@huawei.com> - 4.1-rc2.0.9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add a configuration file in /usr/lib/tmpfiles.d for creating the /run/mdadm directory during the boot process.

* Tue Mar 3 2020 hy <eulerstoragemt@huawei.com> - 4.1-rc2.0.8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add ConditionPathExists in mdmonitor.service for resolving service failure during system startup

* Sun Jan 19 2020 hy <eulerstoragemt@huawei.com> - 4.1-rc2.0.7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add the --syslog option while executing mdadm --scan command in mdmonitor.service

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.1-rc2.0.6
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC:Repackage

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.1-rc2.0.5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:use incremental assembly rules

* Wed Aug 28 2019 zhanghaibo <ted.zhang@huawei.com> - 4.1-rc2.0.4
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESCi:openEuler Debranding

* Wed Aug 21 2019 zhanghaibo <ted.zhang@huawei.com> - 4.1-rc2.0.3
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESCi:openEuler Debranding

* Thu Aug 8 2019 zhanghaibo <ted.zhang@huawei.com> - 4.1-rc2.0.2.h3
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESCi:openEuler Debranding

* Sat Jun 22 2019 zhangsaisai<zhangsaisai@huawei.com> -  4.1-rc2.0.2.h2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport patch from community

* Wed Apr 10 2019 wangjufeng<wangjufeng@huawei.com> -  4.1-rc2.0.2.h1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport patch from community

- Package Initialization
